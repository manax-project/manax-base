# manax-base

manax base django project  

The goal is a microscervice infrastructure built on several django projects.

Each project should be based on django framework, enriched with API (django-rest-framework and GraphQL) to receive communications, Celery to enable async and cron-based tasks, several auth mechanism (LDAP, alluath for social login, X509)

## FEATURES

* For Django 2.1
* Works with Python 3.6
* Twitter [Bootstrap](https://getbootstrap.com/) 4.1.3
* [Django Debug Toolbar](https://github.com/jazzband/django-debug-toolbar) enabled in development mode

## USAGE

### Initialize new project

develop branch

```bash
cookiecutter --overwrite-if-exists https://gitlab.com/manax-project/manax-base.git --checkout develop
```

master branch

```bash
cookiecutter --overwrite-if-exists https://gitlab.com/manax-project/manax-base.git
```

latest release

```bash
cookiecutter --overwrite-if-exists https://gitlab.com/manax-project/manax-base.git --checkout 0.3
```

### virtual env

```bash
cd <project_name>
python3 -m venv venv
source venv/bin/activate
pip install --upgrade pip
```

### initialize repo

```bash
git init
```

### requirements

```bash
pip install -r requirements/local.txt
```

### database

```bash
python manage.py makemigrations
python manage.py migrate
```
### create superuser

```bash
python manage.py createsuperuser
```

### start

```bash
python manage.py runserver
```