# Change Log
All enhancements and patches to Manax Base Django will be documented in this file.
This project adheres to [Semantic Versioning](http://semver.org/).

## [0.4] 2019-01-24

### Added
- gitlab CI
- celery
- TestApp app
  - REST CRUD example
  - Celery Periodic Task example

### Fixed
- PATH for apps folder

## [0.3] 2018-11-29

### Added
- django rest framework

### Fixed
- year detection in cookiecutter.json

## [0.2] 2018-09-07

### Added
- pylint
- theme switchable in templates/base.html
- django-extensions
- django-allauth

## [0.1] 2018-08-18

### Added
- Django 2.1
- Bootstrap 4.1.3
- homepage app
- debug toolbar