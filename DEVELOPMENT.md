# Development Guidelines

## Howto New Release

1. merge develop branch into master branch
2. edit cookiecutter.json removing -dev in "_manax_release"

    - **before** "_manax_release": "x.y-dev"
    - **after**  "_manax_release": "x.y"

3. edit CHAGELOG.md

    - rename [unreleased] in [x.y]
    - add release date 
    - remove unmodified entries

4. commit and push to master branch

    - commit message must be 'release x.y'

5. create tag x.y on master branch
6. push tag
