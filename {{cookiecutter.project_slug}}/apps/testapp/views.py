from django.shortcuts import render
from django.shortcuts import get_object_or_404
from rest_framework.response import Response

from rest_framework.views import APIView
from testapp.models import StringTest
from testapp.serializers import StringTestSerializer

def HomeView(request):
    """
    """

    context = {}
    return render(request, 'testapp/home.html', context) 

class StringTestView(APIView):
    """
    rest_framework.views.APIView example
    """

    def get(self, request, pk = None):
        if pk:
            string = get_object_or_404(StringTest.objects.all(), pk=pk)
            serializer = StringTestSerializer(string)
            return Response({"string": serializer.data})        
        strings = StringTest.objects.all()
        serializer = StringTestSerializer(strings, many = True)
        return Response({"strings": serializer.data})

    def post(self, request):
        string = request.data.get('string')

        # Create an article from the above data
        serializer = StringTestSerializer(data = string)
        if serializer.is_valid(raise_exception=True):
            string_saved = serializer.save()
        return Response({"success": "String '{}' created successfully".format(string_saved.string)})        

    def put(self, request, pk):
        saved_string = get_object_or_404(StringTest.objects.all(), pk=pk)
        data = request.data.get('string')
        serializer = StringTestSerializer(instance=saved_string, data=data, partial=True)
        if serializer.is_valid(raise_exception=True):
            string_saved = serializer.save()
        return Response({"success": "String '{}' updated successfully".format(string_saved.string)})          

    def delete(self, request, pk):
        # Get object with this pk
        string = get_object_or_404(StringTest.objects.all(), pk=pk)
        string.delete()
        return Response({"message": "String with id `{}` has been deleted.".format(pk)},status=204)               