from rest_framework import serializers

from testapp.models import StringTest


class StringTestSerializer(serializers.Serializer):
    """
    """

    string = serializers.CharField(max_length=50)   

    def create(self, validated_data):
        return StringTest.objects.create(**validated_data)          

    def update(self, instance, validated_data):
        instance.string = validated_data.get('string', instance.string)

        instance.save()
        return instance                  