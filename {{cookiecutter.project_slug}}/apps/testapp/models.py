from django.db import models 
 
class StringTest(models.Model):
    """
    """

    string = models.CharField(max_length=50)
    created = models.DateTimeField(auto_now_add=True)

    def __str__(self):
        return "{} {}".format(self.created, self.string)