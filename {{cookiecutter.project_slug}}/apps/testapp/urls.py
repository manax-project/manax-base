from django.urls import path

from testapp.views import HomeView, StringTestView

urlpatterns = [
    # URL pattern for the HomeView
    path(
        route = '',
        view = HomeView,
        #name = 'home'
    ),

    # URL pattern for the StringTestView
    path(
        route = 'strings/',
        view = StringTestView.as_view(),
        #name = 'stringtest'
    ),    

    path(
        route = 'strings/<int:pk>',
        view = StringTestView.as_view(),
        #name ='stringtest'
    ),       
]