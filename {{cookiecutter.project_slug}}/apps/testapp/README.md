# manax install info

## edit ../../config/settings/base.py

LOCAL_APPS = (  
    [...]  
    # TestApp  
    'testapp.apps.TestAppConfig',  
    [...]  
)  

## edit ../../config/urls.py

urlpatterns = [  
    [...]  
    # testapp  
    path('testapp/', include(('testapp.urls', 'testapp'), namespace='testapp')),  
    [...]  
]