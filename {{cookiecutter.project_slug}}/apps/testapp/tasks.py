#from celery.decorators import task

from celery import shared_task

from celery.utils.log import get_task_logger

import time

# import testapp functions

logger = get_task_logger(__name__)

@shared_task 
def test_heartbeat():
     file = open('/tmp/testapp.celeryperiodictask.heartbeat', 'a')
     file.write(str(int(time.time())))
     file.write('\n')
     file.close()