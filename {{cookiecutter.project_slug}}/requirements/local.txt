-r base.txt

# Django Debug Toolbar
# https://github.com/jazzband/django-debug-toolbar
django-debug-toolbar==1.11

# pylint
# https://github.com/PyCQA/pylint
pylint==2.2.2

# django-extensions
# https://github.com/django-extensions/django-extensions
django-extensions==2.1.4