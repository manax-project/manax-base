from .base import *

# Quick-start development settings - unsuitable for production
# See https://docs.djangoproject.com/en/2.1/howto/deployment/checklist/

# SECURITY WARNING: don't run with debug turned on in production!
DEBUG = True

# django-debug-toolbar
# ------------------------------------------------------------------------------

# https://django-debug-toolbar.readthedocs.io/en/latest/installation.html#prerequisites
INSTALLED_APPS += [
    'debug_toolbar',
    # TestApp  
    'testapp.apps.TestAppConfig', 
]

# https://django-debug-toolbar.readthedocs.io/en/latest/installation.html#middleware
MIDDLEWARE += ['debug_toolbar.middleware.DebugToolbarMiddleware']

# https://django-debug-toolbar.readthedocs.io/en/latest/configuration.html#debug-toolbar-config
DEBUG_TOOLBAR_CONFIG = {
    'DISABLE_PANELS': [
        'debug_toolbar.panels.redirects.RedirectsPanel',
    ],
    'SHOW_TEMPLATE_CONTEXT': True,
    'SHOW_COLLAPSED': True,
}

# https://django-debug-toolbar.readthedocs.io/en/latest/installation.html#internal-ips
INTERNAL_IPS = ['127.0.0.1', '10.0.2.2', ]

# insert docker trick
# tricks to have debug toolbar when developing with docker



# /django-debug-toolbar
# ------------------------------------------------------------------------------

# django-extensions
# ------------------------------------------------------------------------------

INSTALLED_APPS += ['django_extensions']

# ------------------------------------------------------------------------------
# /django-extensions